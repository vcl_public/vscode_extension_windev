// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { exec } from 'child_process';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Les commandes pour WinDev sont activées.');

	// Execute windev 
	const disposable = vscode.commands.registerCommand('extension.windev_lance', () => {
		exec('"C:\\PC SOFT\\WINDEV 28\\Programmes\\WINDEV64.exe"', (err, stdout, stderr) => {
			if (stderr) {
				console.error(`Échec de l'exécution : ${err}`);
				console.error(`Sortie d'erreur : ${stderr}`);
				return;
			}
			vscode.window.showInformationMessage('Application lancée avec succès!');
		});
		

	});

	context.subscriptions.push(disposable);
}
